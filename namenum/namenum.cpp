
/*
TASK: namenum
ID: tonymia1
LANG: C++
*/

#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <array>
#include <map>
#include <vector>
#include <sstream>

std::string get_input() {
    std::ifstream fin("namenum.in");
    std::string n;
    fin >> n;
    fin.close();
    return n;
}

std::vector<std::string> get_dict() {
    std::ifstream fin("dict.txt");
    std::vector<std::string> dict;
    std::string str;
    while(std::getline(fin, str)) {
        dict.emplace_back(str);
    }
    fin.close();
    return dict;
}

char char_to_digit(char c) {

    if (c == 'S') {
        return 7+48;
    }

    if (c == 'V') {
        return 8+48;
    }

    if (c == 'Y') {
        return 9+48;
    }

    return ((c-65)/3 + 2) + 48;
}

std::string word_to_number(const std::string& word) {
    std::string res;
    for (const char& c : word) {
        res += char_to_digit(c);
    }
    return res;
}

std::string get_words(const std::string& input, const std::vector<std::string>& dict) {
    std::stringstream res;
    for (auto const& word : dict) {
        auto word_nums = word_to_number(word);
        if (word_nums == input) {
            res << word << std::endl;
        }
    }

    return res.str();
}

void write_output(const std::string& res) {
    std::ofstream fout("namenum.out");

    if (res.length() > 0) {
        fout << res;
    } else {
        fout << "NONE" << std::endl;
    }
}

int main() {
    auto input = get_input();
    auto dict = get_dict();
    auto res = get_words(input, dict);
    write_output(res);


    return 0;
}








