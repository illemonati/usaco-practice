/*
TASK: transform
ID: tonymia1
LANG: C++
*/

#include <iostream>
#include <utility>
#include <vector>
#include <fstream>
#include <tuple>


using namespace std;

typedef vector<vector<char>> grid;


grid rotate_90(grid original) {
    grid new_grid(original.size(), vector<char>(original.size()));
    for (size_t i = 0; i < original.size(); ++i) {
        for (size_t j = 0; j < original.size(); ++ j) {
            new_grid[i][j] = original[original.size()-1 -j][i];
        }
    }
    return new_grid;
}

grid reflect(grid original) {
    grid new_grid(original.size(), vector<char>(original.size()));
    for (size_t i = 0; i < original.size(); ++i) {
        for (size_t j = 0; j < original.size(); ++ j) {
            new_grid[i][j] = original[i][original.size()-1-j];
        }
    }
    return new_grid;
}

uint_fast16_t get_transform(const grid& original, const grid& transformed, bool is_reflect_test=false) {
    grid rotate_grid = rotate_90(original);
    if (rotate_grid == transformed) {
        return 1;
    }
    rotate_grid = rotate_90(rotate_grid);
    if (rotate_grid == transformed) {
        return 2;
    }
    rotate_grid = rotate_90(rotate_grid);
    if (rotate_grid == transformed) {
        return 3;
    }

    if (is_reflect_test) {
        return 8;
    }

    grid reflect_grid = reflect(original);
    if (reflect_grid == transformed) {
        return 4;
    }

    uint_fast16_t reflect_transform = get_transform(reflect_grid, transformed, true);

    static const unsigned rotate_mask = (1 << 1) | (1 << 2) | (1 << 3);

    if ((1 << reflect_transform) & rotate_mask) {
        return 5;
    }

    if (original == transformed) {
        return 6;
    }



    return 7;
}




tuple<grid, grid> get_input() {
    
    ifstream fin("transform.in");
    
    size_t n;
    fin >> n;

    grid original(n), transformed(n);


    for (size_t i = 0; i < n; ++i) {
        vector<char> row(n);
        for (size_t j = 0; j < n; ++j) {
            fin >> row[j];
        }
        original[i] = row;
    }

    for (size_t i = 0; i < n; ++i) {
        vector<char> row(n);
        for (size_t j = 0; j < n; ++j) {
            fin >> row[j];
        }
        transformed[i] = row;
    }

    fin.close();

    return {original, transformed};
}

void write_output(uint_fast16_t result) {
    ofstream fout("transform.out");
    cout << result << endl;
    fout << result << endl;
    fout.close();
}

int main() {
    auto [original, transformed] = get_input();
    write_output(get_transform(original, transformed));

}
