/*
ID: tonymia1
LANG: C++
TASK: beads
*/



#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <sstream>

using namespace std;

class Bead {
public:
    char color = 'w';
    Bead * left = nullptr;
    Bead * right = nullptr;
    explicit Bead(char color): color(color) {}
    Bead() = default;
    ~Bead() {
        right->left = nullptr;
        delete left;
    }
};

class Necklace {
public:
    uint16_t len;
    Bead * starting_bead;
    Necklace(uint16_t len, const string& beads) : len(len) {
        starting_bead = new Bead(beads[0]);
        Bead * prev_bead = starting_bead;
        for (uint16_t i = 1; i < len; ++i) {
            Bead * new_bead = new Bead(beads[i]);
            prev_bead->right = new_bead;
            new_bead->left = prev_bead;
            prev_bead = new_bead;
        }
        prev_bead->right = starting_bead;
        starting_bead->left = prev_bead;
    }
    uint16_t find_longest_chain();
    ~Necklace() {
        delete starting_bead;
    }
};

bool color_match(char color, char new_color) {
    if (color == 'w' || new_color == 'w') {
        return true;
    }
    return new_color == color;
}

uint16_t Necklace::find_longest_chain() {
    Bead * loop_starting_bead = this->starting_bead;
    uint16_t big_sum = 0;
    for (uint16_t i = 0; i < this->len; ++i) {
        Bead *prev_bead = loop_starting_bead;
        char color = prev_bead->color;
        uint16_t left_count = 1;
        for (uint16_t j = 1; j < this->len; ++j) {
            if (color_match(color, prev_bead->left->color)) {
                ++left_count;
                prev_bead = prev_bead->left;
                if (color == 'w') {
                    color = prev_bead->color;
                }
            } else {
                goto END_LEFT;
            }
        }
END_LEFT:
        prev_bead = loop_starting_bead->right;
        color = prev_bead->color;
        uint16_t right_count = 1;
        for (uint16_t j = 1; j < this->len; ++j) {
            if (color_match(color, prev_bead->right->color)) {
                ++right_count;
                prev_bead = prev_bead->right;
                if (color == 'w') {
                    color = prev_bead->color;
                }
            } else {
                goto END_RIGHT;
            }
        }
END_RIGHT:
        uint16_t sum = left_count + right_count;

        if (sum > len) {
            sum = max(left_count, right_count);
        }

        if (sum > big_sum) {
            big_sum = sum;
        }
        loop_starting_bead = loop_starting_bead->right;
//        cout << sum << endl;
    }
    return big_sum;
}

ostream & operator << (ostream &strm, const Necklace &necklace) {

    const Bead * prev_bead = (necklace.starting_bead);
    for (uint16_t i = 0; i < necklace.len; ++i) {
        strm << prev_bead->color;
        prev_bead = prev_bead->right;
    }

    return strm;
}


Necklace get_input() {
    ifstream input("beads.in");
    uint16_t n;
    string beads;
    input >> n;
    input >> beads;
    input.close();
    return Necklace(n, beads);

}

void write_output(uint16_t longest) {
    ofstream out("beads.out");
    out << longest << endl;
    out.close();
}


int main() {
    Necklace necklace = get_input();
//    cout << necklace <<endl;
//    cout << necklace.find_longest_chain() <<endl;
    write_output(necklace.find_longest_chain());
}

