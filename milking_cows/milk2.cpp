/*
TASK: milk2
ID: tonymia1
LANG: C++
*/




#include <iostream>
#include <utility>
#include <vector>
#include <fstream>
#include <tuple>
#include <algorithm>

using namespace std;

class TimeTable {
public:
    vector<uint32_t> start_times;
    vector<uint32_t> end_times;
    TimeTable(vector<uint32_t> start_times, vector<uint32_t> end_times) :
        start_times(std::move(start_times)), end_times(std::move(end_times)) {}
    tuple<uint32_t, uint32_t> get_longest_range();
    static tuple<uint32_t, uint32_t> find_longest_time(vector<bool> block_uses, vector<uint32_t> time_points);
};

tuple<uint32_t, uint32_t> TimeTable::find_longest_time(vector<bool> block_uses, vector<uint32_t> time_points) {
    uint32_t max_milk_time = 0;
    uint32_t temp_milk_time = 0;

    uint32_t max_idle_time = 0;
    uint32_t temp_idle_time = 0;

    for (size_t i = 0; i < block_uses.size(); ++i) {
        if (block_uses[i]) {
            temp_milk_time += time_points[i+1] - time_points[i];

            if (max_idle_time < temp_idle_time) {
                max_idle_time = temp_idle_time;
            }
            temp_idle_time = 0;
        } else {
            if (max_milk_time < temp_milk_time) {
                max_milk_time = temp_milk_time;
            }
            temp_milk_time = 0;

            temp_idle_time += time_points[i+1] - time_points[i];
        }
    }
    if (max_idle_time < temp_idle_time) {
        max_idle_time = temp_idle_time;
    }
    if (max_milk_time < temp_milk_time) {
        max_milk_time = temp_milk_time;
    }

    return {max_milk_time, max_idle_time};
}


tuple<uint32_t, uint32_t> TimeTable::get_longest_range() {
    vector<uint32_t> time_points(this->start_times);
    time_points.insert(time_points.end(), this->end_times.begin(), this->end_times.end());
    sort(time_points.begin(), time_points.end());

    vector<bool> block_uses(time_points.size()-1);

    for(size_t i = 0; i < time_points.size()-1; ++i) {
        auto block_start = time_points[i];
        auto block_end = time_points[i+1];
        for (size_t j = 0; j < start_times.size(); ++j) {
            auto milk_start = start_times[j];
            auto milk_end = end_times[j];
            if (milk_start <= block_start && milk_end >= block_end) {
                block_uses[i] = true;
                break;
            }
            block_uses[i] = false;
        }
    }

    return find_longest_time(block_uses, time_points);
}


TimeTable get_input() {
    ifstream in("milk2.in");
    uint32_t n;
    in >> n;

    vector<uint32_t> start_times(n);
    vector<uint32_t> end_times(n);

    for (uint32_t i = 0; i < n; ++i) {
        in >> start_times[i] >> end_times[i];
    }

    in.close();

    return TimeTable(start_times, end_times);
}

void write_output(uint32_t milk_time, uint32_t idle_time) {
    ofstream out("milk2.out");
    out << milk_time << " " << idle_time << endl;
}

int main() {
    auto time_table = get_input();
    auto [milk, idle] = time_table.get_longest_range();
    write_output(milk, idle);

    return 0;
}



